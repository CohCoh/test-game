return {

    create = function(x, y, image)

        player.x = x
        player.y = y
        player.image = image

    end,


    update = function(dt)

        local xMove, yMove = 0, 0

        if love.keyboard.isDown('left') then
            xMove = -1
        end
        if love.keyboard.isDown('right') then
            xMove = xMove +1
        end

        if love.keyboard.isDown('up') then
            yMove = -1
        end
        if love.keyboard.isDown('down') then
            yMove = yMove +1
        end

        -- Lets move!
        player.x = player.x + (xMove * 360 * dt)
        player.y = player.y + (yMove * 360 * dt)

    end,


    draw = function()

        -- Draw Josef
        love.graphics.draw(player.image, player.x, player.y)

    end
}
